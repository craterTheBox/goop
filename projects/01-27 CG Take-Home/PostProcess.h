#pragma once

//#include <GL/glew.h>
#include <glm/glm.hpp>

#include "shader.h"

class PostProcessor {
public:
	Shader PostProcessShader;
	//Texture2D Texture;
	GLuint Width, Height;

	GLboolean Confuse, Chaos, Shake;

	PostProcessor(Shader shader, GLuint width, GLuint height);

	void BeginRender();

	void EndRender();

	void Render(GLfloat time);
private:
	GLuint MSFBO, FBO;
	GLuint RBO;
	GLuint VAO;

	void initRenderData();
};