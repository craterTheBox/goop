#version 330 core

in vec2 TexCoords;
out vec4 colour;

uniform sampler2D scene;
uniform vec2 offsets[9];
uniform int edge_kernel[9];
uniform float blur_kernel[9];

uniform bool chaos;
uniform book confuse;
uniform bool shake;

void main() {
    colour = vec4(0.0f);
    vec3 sample[9];
    //Sample from texture offsets if using convolution matrix
    if (chaos || shake)
        for (int i = 0; i < 9; i++)
            sample[i] = vec3(texture(scene, TexCoords.st + offsets[i]));

    //Process effects
    if (chaos) {
        for (int i = 0; i < 9; i++)
            colour += vec4(sample[i] * edge_kernel[i], 0.0f);
        colour.a = 1.0f;
    }
    else if (confuse) {
        colour = vec4(1.0 - texture(scene, TexCoords).rgb, 1.0);
    }
    else if (shake) {
        for (int i = 0; i < 9; i++)
            colour += vec4(sample[i] * blur_kernel[i], 0.0f);
        colour.a = 1.0f;
    }
    else {
        colour = texture(scene, TexCoords);
    }
}