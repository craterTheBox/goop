//ICG_Challenge
//Carter Menary - 100700587
//Edward Cao - 100697845


//This code is built off of the GOOP framework with the AudioEngine provided in the Game Sound tutorials


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "TTK/TTKContext.h"
#include "Logging.h"
#include "TTK/GraphicsUtils.h"
#include "TTK/Camera.h"
#include "AudioEngine.h"

#include "layers/LightingLayer.h"
#include "layers/PostLayer.h"
#include "layers/RenderLayer.h"
#include "layers/InstrumentationLayer.h"
#include "layers/SceneBuildLayer.h"
#include "PointLightComponent.h"
#include "ShadowLight.h"
#include "CameraComponent.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include "TTK/Input.h"

#include "imgui.h"
#include "TTK/SpriteSheetQuad.h"

#define LOG_GL_NOTIFICATIONS
/*
	Handles debug messages from OpenGL
	https://www.khronos.org/opengl/wiki/Debug_Output#Message_Components
	@param source    Which part of OpenGL dispatched the message
	@param type      The type of message (ex: error, performance issues, deprecated behavior)
	@param id        The ID of the error or message (to distinguish between different types of errors, like nullref or index out of range)
	@param severity  The severity of the message (from High to Notification)
	@param length    The length of the message
	@param message   The human readable message from OpenGL
	@param userParam The pointer we set with glDebugMessageCallback (should be the game pointer)
*/

void GlDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	std::string sourceTxt;
	switch (source) {
	case GL_DEBUG_SOURCE_API: sourceTxt = "DEBUG"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM: sourceTxt = "WINDOW"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: sourceTxt = "SHADER"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY: sourceTxt = "THIRD PARTY"; break;
	case GL_DEBUG_SOURCE_APPLICATION: sourceTxt = "APP"; break;
	case GL_DEBUG_SOURCE_OTHER: default: sourceTxt = "OTHER"; break;
	}
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:          LOG_INFO("[{}] {}", sourceTxt, message); break;
	case GL_DEBUG_SEVERITY_MEDIUM:       LOG_WARN("[{}] {}", sourceTxt, message); break;
	case GL_DEBUG_SEVERITY_HIGH:         LOG_ERROR("[{}] {}", sourceTxt, message); break;
#ifdef LOG_GL_NOTIFICATIONS
	case GL_DEBUG_SEVERITY_NOTIFICATION: LOG_INFO("[{}] {}", sourceTxt, message); break;
#endif
	default: break;
	}
}

int main() {
	
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	long long memBreak = 0;
	if (memBreak) _CrtSetBreakAlloc(memBreak);

	Logger::Init();

	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 1;
	}

	// Create a new GLFW window
	GLFWwindow* window = glfwCreateWindow(800, 600, "ICG_Practical_Final", nullptr, nullptr);

	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(window);

	// Initialize the TTK input system
	TTK::Input::Init(window);

	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 2;
	}

	// Display our GPU and OpenGL version
	std::cout << glGetString(GL_RENDERER) << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(GlDebugMessage, nullptr);

	TTK::Graphics::SetCameraMode3D(800, 600);
	glfwSetWindowSizeCallback(window, [](GLFWwindow* window, int width, int height) {
		TTK::Graphics::SetCameraMode3D(width, height);
		});
	TTK::Graphics::SetBackgroundColour(0.0f, 0.0f, 0.0f);
	TTK::Graphics::SetDepthEnabled(true);

	TTK::Camera camera;
	camera.cameraPosition = glm::vec3(0.0f, 0.0f, -10.0f);
	camera.forwardVector = glm::vec3(0.0f, 0.0f, 1.0f);

	glm::vec2 lastMousePos, mousePos;
	lastMousePos = TTK::Input::GetMousePos();

	TTK::Graphics::InitImGUI(window);

	float lastFrame = glfwGetTime();
	
	//AudioEngine stuff
	AudioEngine ae;
	ae.Init();
	ae.LoadBank("Master", FMOD_STUDIO_LOAD_BANK_NORMAL);
	ae.LoadEvent("beep", "{c86b2f63-2d58-49c7-997f-c2b68a93aaaf}");
	ae.PlayEvent("beep");
	
	//Lighting Layer
	LightingLayer ll;
	//ll.Initialize(); 

	//Objects
	glm::vec3 froge = { 0.0f, -4.0f, 0.0f };
	glm::vec3 car1 = { -6.0f, -2.0f, 0.0f };
	glm::vec3 car2 = { 4.0f, 2.0f, 0.0f };

	//Car Velocities
	glm::vec3 car1Velocity = { 4.0f, 0.0f, 0.0f };
	glm::vec3 car2Velocity = { -4.0f, 0.0f, 0.0f };

	//Colours
	glm::vec4 green = { 0.0f, 0.5f, 0.0f, 1.0f };
	glm::vec4 white = { 1.0f, 1.0f, 1.0f, 1.0f };

	while (!glfwWindowShouldClose(window)) {
		// Poll for events from windows (clicks, keypressed, closing, all that)
		glfwPollEvents();

		float thisFrame = glfwGetTime();
		float dt = thisFrame - lastFrame;

		// Clear our screen every frame
		TTK::Graphics::ClearScreen();

		//Gameplay stuff
		{
			//Move forward
			if (TTK::Input::GetKeyDown(TTK::KeyCode::W)) {
				froge.y += 1.0f * dt;
			}

			//update car position here
			car1 += car1Velocity * dt;
			car2 += car2Velocity * dt;

			//out of bounds reset for the cars
			if (car1.x > 10.0f) {
				car1.x = -10.0f;
			}
			if (car2.x < -10.0f) {
				car2.x = 10.0f;
			}

			//Collision
			if (froge.x < car1.x + 0.25f && froge.x + 0.25f > car1.x&& froge.y < car1.y + 0.25f && froge.y + 0.25f > car1.y) {
				car1Velocity *= 0.0f;
				car2Velocity *= 0.0f;
				ae.PlayEvent("beep");
			}
			if (froge.x < car2.x + 0.25f && froge.x + 0.25f > car2.x&& froge.y < car2.y + 0.25f && froge.y + 0.25f > car2.y) {
				car1Velocity *= 0.0f;
				car2Velocity *= 0.0f;
				ae.PlayEvent("beep");
			}
		}

		//Graphics stuff
		{
			//Draw the objects
			TTK::Graphics::DrawCube(froge, 0.5f, green);
			TTK::Graphics::DrawCube(car1, 0.5f, white);
			TTK::Graphics::DrawCube(car2, 0.5f, white);

			//Lights
			ll.Update();
		}

		//Update Audio Engine
		ae.Update();

		camera.update();
		TTK::Graphics::SetCameraMatrix(camera.ViewMatrix);

		TTK::Graphics::EndFrame();
		TTK::Graphics::BeginGUI();
		TTK::Graphics::EndGUI();

		// Present our image to windows
		glfwSwapBuffers(window);

		TTK::Input::Poll();

		lastFrame = thisFrame;
	}

	glfwTerminate();


	//Shut down audio engine and lighting layer
	ae.Shutdown();
	ll.Shutdown();

	TTK::Graphics::ShutdownImGUI();
	TTK::Input::Uninitialize();
	TTK::Graphics::Cleanup();
	Logger::Uninitialize();

	return 0;
}